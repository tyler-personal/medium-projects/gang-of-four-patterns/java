package Patterns.Strategy.Java8WithoutStrategy;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class Customer {
    private final List<Integer> drinks = new ArrayList<>();
    private Function<Integer, Integer> billingStrategy;

    public Customer(Function<Integer, Integer> billingStrategy) {
        this.billingStrategy = billingStrategy;
    }

    public void addDrink(int price, int quantity) {
        drinks.add(billingStrategy.apply(price * quantity));
    }

    public int getBill() {
        int sum = drinks.stream().mapToInt(v -> v).sum();
        drinks.clear();
        return sum;
    }

    public void setBillingStrategy(Function<Integer, Integer> billingStrategy) {
        this.billingStrategy = billingStrategy;
    }
}
