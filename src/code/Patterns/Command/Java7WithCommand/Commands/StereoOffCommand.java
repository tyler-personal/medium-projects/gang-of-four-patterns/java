package Patterns.Command.Java7WithCommand.Commands;

import Patterns.Command.Java7WithCommand.Entities.Stereo;

public class StereoOffCommand implements Command {
    private final Stereo stereo;

    public StereoOffCommand(Stereo stereo) {
        this.stereo = stereo;
    }

    public void execute() {
        stereo.off();
    }
}
