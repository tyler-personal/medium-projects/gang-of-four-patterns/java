package Patterns.Strategy.Java8WithoutStrategy;

import java.util.function.Function;

public class Main {
    public static void main(String[] arguments) {
        Function<Integer, Integer> normal = p -> p;
        Function<Integer, Integer> happyHour = p -> p / 2;

        Customer firstCustomer = new Customer(normal);

        firstCustomer.addDrink(100, 1);

        firstCustomer.setBillingStrategy(happyHour);
        firstCustomer.addDrink(100, 2);

        Customer secondCustomer = new Customer(happyHour);
        secondCustomer.addDrink(80, 1);
        System.out.println(firstCustomer.getBill());

        secondCustomer.setBillingStrategy(normal);
        secondCustomer.addDrink(130, 2);
        secondCustomer.addDrink(250, 1);
        System.out.println(secondCustomer.getBill());
    }
}