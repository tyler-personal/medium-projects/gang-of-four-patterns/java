package Patterns.Command.Java8WithoutCommand.Entities;

public class RemoteControl {
    private Runnable button;

    public RemoteControl() { }

    public void setCommand(Runnable command) {
        button = command;
    }

    public void pressButton() {
        button.run();
    }
}
