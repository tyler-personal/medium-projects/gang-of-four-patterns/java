package Patterns.Command.Java7WithCommand.Commands;

import Patterns.Command.Java7WithCommand.Entities.Light;

public class LightOnCommand implements Command {
    private final Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    public void execute() {
        light.on();
    }
}
