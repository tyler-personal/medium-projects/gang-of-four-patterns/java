package Patterns.Strategy.Java7WithStrategy;

public interface BillingStrategy {
    int getPrice(int rawPrice);

    BillingStrategy normalStrategy = new NormalStrategy();
    BillingStrategy happyHourStrategy = new HappyHourStrategy();

    class NormalStrategy implements BillingStrategy {
        @Override
        public int getPrice(int rawPrice) {
            return rawPrice;
        }
    }

    class HappyHourStrategy implements BillingStrategy {
        @Override
        public int getPrice(int rawPrice) {
            return rawPrice / 2;
        }
    }
}
