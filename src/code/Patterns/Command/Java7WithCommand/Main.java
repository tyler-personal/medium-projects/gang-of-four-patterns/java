package Patterns.Command.Java7WithCommand;

import Boundary.IO;
import Patterns.Command.Java7WithCommand.Commands.*;
import Patterns.Command.Java7WithCommand.Entities.*;
import Presentation.PresentationDependencies;

class Main {
    public static void main(String[] args) {
        RemoteControl remote = new RemoteControl();
        IO io = PresentationDependencies.io;

        Light light = new Light(io);
        Stereo stereo = new Stereo(io);

        remote.setCommand(new LightOnCommand(light));
        remote.pressButton();
        remote.setCommand(new StereoOnWithCDCommand(stereo));
        remote.pressButton();
        remote.setCommand(new StereoOffCommand(stereo));
        remote.pressButton();
    }
}