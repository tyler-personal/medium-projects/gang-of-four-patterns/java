package Patterns.Strategy.Java7WithStrategy;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private final List<Integer> drinks = new ArrayList<>();
    private BillingStrategy strategy;

    public Customer(BillingStrategy strategy) {
        this.strategy = strategy;
    }

    public void addDrink(int price, int quantity) {
        drinks.add(strategy.getPrice(price * quantity));
    }

    public int getBill() {
        int sum = 0;
        for (int drink : drinks) {
            sum += drink;
        }
        drinks.clear();
        return sum;
    }

    public void setStrategy(BillingStrategy strategy) {
        this.strategy = strategy;
    }
}
