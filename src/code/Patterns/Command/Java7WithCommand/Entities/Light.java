package Patterns.Command.Java7WithCommand.Entities;

import Boundary.IO;

public class Light {
    private final IO io;

    public Light(IO io) {
        this.io = io;
    }

    public void on() {
        io.show("Light is on");
    }

    public void off() {
        io.show("Light is off");
    }
}